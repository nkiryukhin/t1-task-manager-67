package ru.t1.nkiryukhin.tm.exception.field;

public final class UserIdEmptyException extends AbstractFieldException {

    public UserIdEmptyException() {
        super("Error! Task id is empty...");
    }

}
