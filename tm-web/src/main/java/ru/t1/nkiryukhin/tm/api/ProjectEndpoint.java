package ru.t1.nkiryukhin.tm.api;

import org.springframework.web.bind.annotation.*;
import ru.t1.nkiryukhin.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@RequestMapping("/api/projects")
public interface ProjectEndpoint {

    @WebMethod
    @PostMapping("/clear")
    void clear();

    @WebMethod
    @GetMapping("/count")
    long count() throws Exception;

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll(
            @WebParam(name = "projects", partName = "projects")
            @RequestBody List<Project> projects
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody Project project
    );

    @WebMethod
    @PostMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/findAll")
    Collection<Project> findAll();

    @WebMethod
    @GetMapping("/findById/{id}")
    Project findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/save")
    void save(
            @WebParam(name = "project", partName = "project")
            @RequestBody Project project
    );

}