package ru.t1.nkiryukhin.tm.endpoint;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.nkiryukhin.tm.api.TaskEndpoint;
import ru.t1.nkiryukhin.tm.model.Task;
import ru.t1.nkiryukhin.tm.repository.TaskRepository;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.t1.nkiryukhin.tm.api.TaskEndpoint")
public class TaskEndpointImpl implements TaskEndpoint {

    @Autowired
    private TaskRepository taskRepository;

    @Override
    @WebMethod
    @PostMapping("/clear")
    public void clear() {
        taskRepository.deleteAll();
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return taskRepository.count();
    }

    @Override
    @WebMethod
    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") @NotNull final String id) {
        taskRepository.deleteById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(@RequestBody @NotNull final Task task) {
        taskRepository.deleteById(task.getId());
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(@RequestBody @NotNull List<Task> tasks) {
        for (Task task : tasks) {
            taskRepository.deleteById(task.getId());
        }
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(@PathVariable("id") @NotNull final String id) {
        return taskRepository.findById(id).isPresent();
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<Task> findAll() {
        return taskRepository.findAll();
    }

    @Nullable
    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public Task findById(@PathVariable("id") @NotNull final String id) {
        return taskRepository.findById(id).orElse(null);
    }

    @Override
    @WebMethod
    @PostMapping("/save")
    public void save(@RequestBody @NotNull final Task task) {
        taskRepository.save(task);
    }

}