package ru.t1.nkiryukhin.tm.api;

import org.springframework.web.bind.annotation.*;
import ru.t1.nkiryukhin.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@RequestMapping("/api/tasks")
public interface TaskEndpoint {

    @WebMethod
    @PostMapping("/clear")
    void clear();

    @WebMethod
    @GetMapping("/count")
    long count();

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll(
            @WebParam(name = "tasks", partName = "tasks")
            @RequestBody List<Task> tasks);

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody Task task);

    @WebMethod
    @GetMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id);

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/findAll")
    Collection<Task> findAll();

    @WebMethod
    @GetMapping("/findById/{id}")
    Task findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable String id
    );

    @WebMethod
    @PostMapping("/save")
    void save(
            @WebParam(name = "task", partName = "task")
            @RequestBody Task task
    );

}